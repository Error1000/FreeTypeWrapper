#ifndef FREETYPEGLYPHMETRICS_H
#define FREETYPEGLYPHMETRICS_H

struct FreeTypeGlyphMetrics{
unsigned int width, height;
unsigned int advanceX, advanceY;
unsigned int bearingX, bearingY;
float offsetX, offsetY;

FreeTypeGlyphMetrics(){}
};

#endif
