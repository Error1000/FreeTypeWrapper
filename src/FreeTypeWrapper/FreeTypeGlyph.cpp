#include "FreeTypeGlyph.h"


FreeTypeGlyph::FreeTypeGlyph( const FT_GlyphSlot& glph){

    if(glph == nullptr) throw FreeError("GlyphSlot is null!");
    metrics.width = glph->bitmap.width;
    metrics.height = glph->bitmap.rows;
    metrics.advanceX = glph->advance.x / 64; //Divide by 64 to get size in pixels
    metrics.advanceY = glph->advance.y / 64; //Divide by 64 to get size in pixels
    metrics.bearingX = glph->metrics.horiBearingX / 64;
    metrics.bearingY = glph->metrics.horiBearingY / 64;

    if(metrics.bearingX > INT_MAX - 100)
       metrics.offsetX = (int)(metrics.bearingX + metrics.width/2);
    else
       metrics.offsetX = (float)(metrics.bearingX + metrics.width/2.0f);

    if(metrics.bearingY > INT_MAX - 100)
       metrics.offsetY = (int)(metrics.bearingY - metrics.height/2);
    else
       metrics.offsetY = (float)(metrics.bearingY - metrics.height/2.0f);


  FreeTypeGlyph::bitmapBuffer = new unsigned char[FreeTypeGlyph::metrics.width * FreeTypeGlyph::metrics.height * 4];
  unsigned int j = 0;
  for(unsigned int i = 0; i < FreeTypeGlyph::metrics.width * FreeTypeGlyph::metrics.height; i++){
    FreeTypeGlyph::bitmapBuffer[j] = 0;
    FreeTypeGlyph::bitmapBuffer[j+1] = 0;
    FreeTypeGlyph::bitmapBuffer[j+2] = 0;
    FreeTypeGlyph::bitmapBuffer[j+3] = glph->bitmap.buffer[i];
   j += 4;
  }

}

