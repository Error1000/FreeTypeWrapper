#ifndef FREETYPEFONT_H
#define FREETYPEFONT_H
#include "FreeTypeLib.h"
#include "FreeTypeGlyph.h"
#include <string>
#include <limits>

class FreeTypeFont{
private:
FT_Face font;

public:

explicit FreeTypeFont(const std::string& fontLocation, const unsigned int faces = 0);

FreeTypeGlyph renderCharacter(const int c, const unsigned int renderQualityWidth = 100, const unsigned int renderQualityHeight = 100);
FreeTypeGlyph** renderCharacterRange(const int beginc = CHAR_MIN, const int endc = CHAR_MAX, const unsigned int renderQualityWidth = 100, const unsigned int renderQualityHeight = 100);
const unsigned int getNumberOfChars();
};

#endif // FREETYPEFONT_H_INCLUDED
