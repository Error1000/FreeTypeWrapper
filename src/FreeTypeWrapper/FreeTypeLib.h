#ifndef FREETYPELIB_H
#define FREETYPELIB_H

#include <ft2build.h>
#include FT_FREETYPE_H


class FreeTypeLib{
public:
  static FT_Library ftlib;

  static void init();
  inline static FT_Library& getLibrary(){ return ftlib; }

};


#endif // FREETYPELIB_H
