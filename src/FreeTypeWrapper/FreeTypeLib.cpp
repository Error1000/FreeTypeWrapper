#include "FreeTypeLib.h"
#include "FreeError.h"

FT_Library FreeTypeLib::ftlib = FT_Library();

void FreeTypeLib::init(){
    if(FT_Init_FreeType(&FreeTypeLib::ftlib)){
        throw FreeError("Couldn't initialize library!");
    }
}
