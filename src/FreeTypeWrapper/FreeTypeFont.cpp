#include "FreeTypeFont.h"
#include <string>

FreeTypeFont::FreeTypeFont(const std::string& fontLocation, const unsigned int faces){

 if(FT_New_Face( FreeTypeLib::getLibrary(), fontLocation.c_str(), faces, &(FreeTypeFont::font) )){
    throw FreeError("Font couldn't be loaded!");
 }

}


FreeTypeGlyph FreeTypeFont::renderCharacter(const int c, const unsigned int renderQualityWidth, const unsigned int renderQualityHeight){
 FT_Set_Pixel_Sizes(FreeTypeFont::font, renderQualityWidth, renderQualityHeight);
 if(FT_Load_Char(FreeTypeFont::font, c, FT_LOAD_RENDER)){
    throw FreeError("Can't render character!");
 }
 return FreeTypeGlyph(FreeTypeFont::font->glyph);
}

FreeTypeGlyph** FreeTypeFont::renderCharacterRange(const int beginc, const int endc, const unsigned int renderQualityWidth, const unsigned int renderQualityHeight){
 FT_Set_Pixel_Sizes(FreeTypeFont::font, renderQualityWidth, renderQualityHeight);
 FreeTypeGlyph** arr = new FreeTypeGlyph*[(endc - beginc) + 1];

 for(int i = beginc; i <= endc; i++){
    //Note: we don't cast i because c++ will automgically cast it for us t owhat th efunction needs
  if(FT_Load_Char(FreeTypeFont::font, i, FT_LOAD_RENDER)){
     for(int j = 0; j < (endc - beginc) + 1; j++) delete arr[j];
     delete arr;
     throw FreeError(std::string("Can't render character ") + std::string( "#" + std::to_string(i) ) + std::string(" !") );
  }
  arr[i-beginc] = new FreeTypeGlyph(FreeTypeFont::font->glyph);
 }

 return arr;
}

const unsigned int FreeTypeFont::getNumberOfChars(){
 unsigned int index;
 unsigned long long cc = FT_Get_First_Char(FreeTypeFont::font, &index);
 for(;;cc++){

    FT_Get_Next_Char(FreeTypeFont::font, cc, &index);
    if(index == 0) break;
 }
 return cc;
}
