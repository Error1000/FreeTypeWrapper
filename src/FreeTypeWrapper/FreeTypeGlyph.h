#ifndef FREETYPEGLYPH_H
#define FREETYPEGLYPH_H

#include "FreeTypeLib.h"
#include "FreeError.h"
#include "FreeTypeGlyphMetrics.h"

class FreeTypeGlyph{
private:
//We use variables instead of storing the glyphs slot, because the glyph slot is basically a pointer which means that if a glyph slot is set to a font's renderd glyph, the glyph slot can change the glyph it's pointing to, so it's easier to do it this way
FreeTypeGlyphMetrics metrics;
unsigned char* bitmapBuffer;

public:
FreeTypeGlyph( const FT_GlyphSlot& glph);

~FreeTypeGlyph(){ delete[] FreeTypeGlyph::bitmapBuffer; }

const unsigned int getWidth() const{ return FreeTypeGlyph::metrics.width; }
const unsigned int getHeight() const{ return FreeTypeGlyph::metrics.height; }
unsigned char* getData() const{ return FreeTypeGlyph::bitmapBuffer; }
FreeTypeGlyphMetrics getMetrics() const{ return metrics; }
};

#endif // FREETYPEGLYPH_H
