#include "../../FreeTypeWrapper/Include.h"
#include <OpenGLWrapper/Include.h>
#include <OpenGLWrapperUtils/Include.h>
#include <vector>
#include <string>

using namespace UserUtils;

void setup();
void render();
void onExit();

void onResize();





FreeTypeFont* font;
FreeTypeGlyphMetrics* glyphs;

std::wstring s = L"The quick brown fox jumped over the lazy dog."; //"ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz 1234567890!@#$%^&*()_+|-=\~`";
unsigned int sWidth = 0;
unsigned int sHeight = 0;
unsigned int chars = 0;
unsigned int cmax = 0, cmin = 0;

TexturedDrawableRectangle* cursor;
Texture** texArr;
mat3 projection;


void onResize(){
projection = glm::ortho(-(float)Window::getWidth()/2, (float)Window::getWidth()/2, -(float)Window::getHeight()/2, (float)Window::getHeight()/2);
}

int main(){
 try{
  Window::setResizeable(true);
  Window::useVSync(true);
  //The width will be set later to the size of the string
  Window::setWindowResizeCallback(onResize);
  Wrapper::initWrapper(setup, render, onExit);
 }catch(APIError a){
  Logger::log(a.message, Level::Error);
  return 0;
 }catch(WindowError e){
  Logger::log(e.message, Level::Error);
  return 0;
 }

 Wrapper::runWrapper();
}

void setup(){
 backgroundColor(255);

 try{
  FreeTypeLib::init();
 }catch(FreeError e){
  Logger::log(e.message, Level::Error);
  return;
 }

 try{
  DefaultShader2D::init();
 }catch(ShaderError se){
  Logger::log(se.message, Level::Error);
  return;
 }

 const static string fntName = "/NotoSans-Regular.ttf";
 try{
  font = new FreeTypeFont(getCurrentProgramDir() + fntName);
 }catch(FreeError e){
  Logger::log(e.message, Level::Error);
  Logger::log("Maybe the font is corrputed, or the path " + getCurrentProgramDir()+fntName + " is wrong?", Level::Info);
  return;
 }

 //Find biggest and smallest character
 //Kind of a stupid approach that dosen't lest us change the string later, but it works
 for(wchar_t cc : s) if((unsigned int)cc > cmax) cmax = (unsigned int) cc;
 cmin = cmax;
 for(wchar_t cc : s) if((unsigned int)cc < cmin) cmin = (unsigned int) cc;
 chars = (cmax-cmin) +1;

 //Log the amount of chars that the render character range function needs to render
 Logger::log(chars);

 texArr = new Texture*[chars];
 glyphs = new FreeTypeGlyphMetrics[chars];
 const static unsigned int renderingCharWidth = 30;
 const static unsigned int renderingCharHeight = 30;

 FreeTypeGlyph** actualglyphs;
 try{
   actualglyphs = font->renderCharacterRange(cmin, cmax, renderingCharWidth, renderingCharHeight);
 }catch(FreeError e){
  Logger::log(e.message);
  return;
 }

 for(unsigned int i = 0; i < chars; i++){

     glyphs[i] = actualglyphs[i]->getMetrics();

     Image img;
     img.data = actualglyphs[i]->getData();
     img.width = actualglyphs[i]->getWidth();
     img.height = actualglyphs[i]->getHeight();
     texArr[i] = new Texture(img);

 }

 delete font;
 for(unsigned int i = 0; i < chars; i++)delete actualglyphs[i];
 delete actualglyphs;


 unsigned int iter = 0;
 for(wchar_t ch : s){
  sWidth += glyphs[ch - cmin].advanceX;
  sHeight += glyphs[ch - cmin].height;
  iter++;
 }
 sHeight /= iter;


 try{
  Window::setWidth(sWidth);
 }catch(WindowError w){
  //Maybe try to recover?
  try{
   Window::setWidth(100);
  }catch(WindowError we){
   //Fatal fail
   return;
  }
  Logger::log(w.message);
 }


 try{
  Window::setHeight(renderingCharHeight + 20);
 }catch(WindowError w){
  //Maybe try to reover?
  try{
   Window::setHeight(100);
  }catch(WindowError we){
   //Fatal fail
   return;
  }
  Logger::log(w.message);
 }


 cursor = new TexturedDrawableRectangle(vec2(1, 1), vec2(0, 0));

 projection = glm::ortho(-(float)Window::getWidth()/2, (float)Window::getWidth()/2, -(float)Window::getHeight()/2, (float)Window::getHeight()/2);
 DefaultShader2D::bindShader();
 cursor->getShape()->bindShape();
 DefaultShader2D::useTexture(true);
 DefaultShader2D::setSampler(0);

 try{
  DefaultShader2D::getShader()->validateShader();
 }catch(ShaderError validationError){
  Logger::log(validationError.message, Level::Error);
  Logger::log("OpenGL shader validation error!");
  return;
 }

}


void render(){
if(Window::isKeyPressed(GLFW_KEY_ESCAPE))exitWrapper();
//Reset "cursor" position


cursor->setPosition(vec2(-(int)sWidth/2, -(int)sHeight/2 ));

for(unsigned int k = 0; k < s.size(); k++){
const wchar_t cc = s[k];
if((unsigned int)cc > cmax){
    Logger::log( std::string("Character #") + to_string(cc) + std::string(" dosen't seem to be supported by this font?"), Level::Error);
    continue;
}
const FreeTypeGlyphMetrics metric = glyphs[cc-cmin];


if(metric.width != 0 && metric.height != 0){
texArr[cc-cmin]->bindTexture(0);

//Add offset to current character according to metrics
cursor->move( vec2( metric.offsetX, metric.offsetY ));

cursor->setSize(vec2( metric.width, metric.height ));
DefaultShader2D::getShader()->setMatrixUniform(DefaultShader2D::getTransformationMatrixId(), projection * Camera2D::getViewMatrix() * cursor->getModelMatrix());
cursor->renderObject();

//Remove offset of current character
cursor->move( vec2( -metric.offsetX, -metric.offsetY ));

//Advance to next character
cursor->move( vec2( metric.advanceX, metric.advanceY));
}else{
//Ignore character and just sdvance without rendering
//Advance to next character
cursor->move( vec2( metric.advanceX, metric.advanceY));
}

}


}

void onExit(){

delete cursor;
DefaultShader2D::destroy();
delete font;

}
