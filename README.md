# FreeTypeWrapper
This is a simple wrapper for the freetypewrapper library, nothing great but it'll do.

NOTE: This git repository uses submoduels so to clone it you need to add the arguments: --recurse-submodules -j8 to the git clone command!!!

NOTE: Since this repository uses submodules DO NOT download it using the web interface it does not download submodules instead clone the project with the arguments: --recurse-submodules -j8 !!!


# How to build the release (build release library binary)
 Note: The automatron commands must be run from the project folder where this README is located!!!

 # GNU/Linux (using make to build and the default package manager to get libraries and install stuff)
  Note: This command will download the required libraries to build. 

  - Ubuntu: sudo ./automatron9000/automatron9000.sh -m release -o ubuntu -f makefile-linux -l "$(cat Libraries/linux/ubuntu/ilibs)"

  - Arch: sudo ./automatron9000/automatron9000.sh -m release -o arch -f makefile-linux -l "$(cat Libraries/linux/arch/ilibs)"
 
 # MacOS (using make to build and brew package manager to get libraries and install stuff)
  Note: This command will download and install the required applications needed to build and install brew. 

  - ./automatron9000/automatron9000.sh -m release -o macos -f makefile-macos -l "$(cat Libraries/macos/ilibs)"

 # Windows (using make)
  Note: This command will download and install the required applications needed to build this project and it will also install chocolatey. 
  
  - Go to Libraries\windows, unzip and put all the contents of the zipps in the same folder
  - Open cmd as administrator
  - Run: automatron9000\automatron9000.bat -m release -o windows -l vcruntime140

# How to test build (build debug executable binary)

Note: The automatron commands must be run from the project folder where this README is located!!!

 # GNU/Linux (using make to build and assuming you already built the release and have all the stuff necessary from building the release)
  - Ubuntu: ./automatron9000/automatron9000.sh -m debug -o ubuntu

  - Arch: ./automatron9000/automatron9000.sh -m debug -o arch

 # MacOS (using make to build and assuming you already built the release and have all the stuff necessary from building the release)
  - ./automatron9000/automatron9000.sh -m debug -o macos

 # Windows (using make to build and assuming you already built the release and have all the stuff necessary from building the release)
  - Go to Libraries\windows, make sure that all zips are unzipped if not, unzip and put all the contents of the zipps in the same folder
  - Open cmd as administrator
  - Run: automatron9000\automatron9000.bat -m debug -o windows
